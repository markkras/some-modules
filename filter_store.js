
// @info list of filter field's names

/* @params
'arrLotFilter_CATEGORY[]', // Вид имущества
'arrLotFilter_3', // Поиск по номеру лота
'arrLotFilter_15_MIN', // Текущая цена
'arrLotFilter_15_MAX', // Текущая цена
'arrLotFilter_PUBLICATION_DATE_START', // Дата публикации
'arrLotFilter_PUBLICATION_DATE_END', // Дата публикации
'arrLotFilter_98', // Способ реализации
'arrLotFilter_7', // Местонахождение
'arrLotFilter_12', // Поиск по ЭП
'arrLotFilter_TRADING_NUMBER', // № торга на ЭП
'arrLotFilter_4', // Наименование ФО/Агентство
'arrLotFilter_TRADING_DATE_START', // Дата (период) реализации
'arrLotFilter_TRADING_DATE_END', // Дата (период) реализации
'arrLotFilter_NOVELTY',


arrLotFilter_CATEGORY - lot_section_id
arrLotFilter_3 - lot_number
arrLotFilter_15_MIN - price_min
arrLotFilter_15_MAX - price_max
arrLotFilter_PUBLICATION_DATE_START - publication_date_start
arrLotFilter_PUBLICATION_DATE_END - publication_date_end
arrLotFilter_98 - tender_section_id
arrLotFilter_7 - region_id
arrLotFilter_12 - etp_id
arrLotFilter_TRADING_NUMBER - etp_number
arrLotFilter_TRADING_NUMBER - etp_number
arrLotFilter_4 - fo_id
arrLotFilter_TRADING_DATE_START - implementation_date_start
arrLotFilter_TRADING_DATE_END - implementation_date_end
arrLotFilter_NOVELTY - novelty

*/


/* 
    @info 
{
    name: '',
    link: [],
    id: ??
},
{
    name: '',
    link: [],
    id: ??
},

*/


// global store 

const filter_store = [];

// global function to initialize filter_store array; 

const setFilterStore = () =>  {
    $('.js-form-main .form-main__body input').each(function (idx, el) { 
        let name = $(el).attr('name');
        let id = $(el).attr('data-id');
        let link = [];
        if(typeof(name) !== 'undefined'){
            // filter_store.splice(0, filter_store.length - 1, {name, link})
            filter_store.push({name, link});
        }
    });    
    return filter_store;
}

// function for changing filter store global state   
function mutationSet(name, link){    
    let target = filter_store.find(x => x.name == name);
    // do not push this link if it's already in link array
    if(!target.link.includes(link)){
        target.link.push(link);
        return target;
    }
}


const getActiveIds = (list) => list.map(x => x.id);


// get current serialized filter_store array 

const getFormResultQuery = () => {
    return filter_store.filter(element => element.link.length > 0) 
    .map(x => {
        const res = Object.values(x.link).map(item => {
            return item = x.name + '=' + item.toString() + '&';
        }).join('').slice(0, -1);
        return res;
    })
    .reduce((acc, val) => {
        return acc + val + '&';
    }, '?');
}


// update current get param

function updateURL(formResultQuery, locationSubmit = '') {
    if (history.replaceState) {
        let newUrl = '';
        if(formResultQuery.length > 1){
            newUrl = locationSubmit + formResultQuery.slice(0, -1) + '&set_filter=Y';
            history.replaceState(null, null, newUrl);
        } else {
            history.replaceState(null, null, window.location.href + locationSubmit);
        }
    }
    else {
        console.warn('History API не поддерживается');
    }
}

$(document).ready(() => {
    $('.js-form-main').on('submit', function(e){
        if(window.location.href.includes('sales')){
            e.preventDefault();
            updateURL(getFormResultQuery());
            window.location.reload(true);
        } else {
            e.preventDefault();
            updateURL(getFormResultQuery(), 'sales/');
            $(this).submit();
        }
    });
});


