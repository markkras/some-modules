let selectPropertyClass, 
    selectAgencyClass, 
    selectRegionsClass,
    selectRegionsSignature,
    selectRegionsRealization;
let calendarPublication, 
    calendarRealization;

// Массив где хранится пересчитанные данны
let recount = [];

let recount_filter_data = {
    // lot_section_id: [59, 62, 65, 70],
    lot_section_id: [],
    // fo_id: [135796],
    fo_id: [],
    // region_id: [311]
    region_id: [],
    novelty: false,
};



/* 
@info http://t1m0n.name/air-datepicker/docs/index-ru.html
*/
/**
 * @param  { Element } $('.js-calendar-from-1')
 * @param  { Element } $('.js-calendar-to-1')
 * @param  { Date } minDate
 * @param  { Date } maxDate
*/
class fCalendar{
    constructor(from, to, minDate, maxDate){
        this.from = from;
        this.to = to;
        this.options = {
            minDate, 
            maxDate,  
            options: {
                closeOnSelect: false,
                allowClear: true,
                multiple: true,
                theme: 'default',
                tags: false,
                data: []
            },
            language: "ru",
            autoClose: true,
            range: false,
            multipleDatesSeparator: "-",
            prevHtml: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 20"><path fill="currentColor" d="M.44 2.877A1.54 1.54 0 1 1 2.64.723l9.203 9.407-9.202 9.407a1.54 1.54 0 0 1-2.202-2.154l7.096-7.253L.439 2.877z"/></svg>',
            nextHtml: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 20"><path fill="currentColor" d="M.44 2.877A1.54 1.54 0 1 1 2.64.723l9.203 9.407-9.202 9.407a1.54 1.54 0 0 1-2.202-2.154l7.096-7.253L.439 2.877z"/></svg>',
            onSelect:  (element, date, inst) => {  
                if(inst.$el.hasClass('js-calendar-from')){
                    this.compareFrom(element);
                } else {
                    this.compareTo(element);
                }
            }
        };
        
        this.from.datepicker(this.options);
        this.from.data('datepicker'); 
        this.to.datepicker(this.options);
        this.to.data('datepicker'); 
    }
    compareFrom(element) {
        if(!this.to.val()){
            this.from.val(element);                        
        } else {
            if(this.from.val() > this.to.val()) {
                this.to.data('datepicker').selectDate(this.from.data('datepicker').lastSelectedDate);
            } else {
                this.from.val(element);
            }
        }
    }
    compareTo(element) {
        if(!this.from.val()){
            this.to.val(element);
        } else {
            if(this.to.val() < this.from.val()){
                this.from.data('datepicker').selectDate(this.to.data('datepicker').lastSelectedDate);
            } else {
                this.to.val(element);
            } 
        }    
    }
    updatePosition(){
        if(this.from.length > 0 && this.to.length > 0){
            this.from.data('datepicker').update('position');
            this.to.data('datepicker').update('position');

        }
    }
}



var customScroll = {
    init: function () {
        var $parent = $('select');
        var options = {
            theme: "dark-thick",
        }
        $parent.on('select2:open', function (e) {
            var $target = $('.select2-results__options');
            $target.mCustomScrollbar('destroy');
            setTimeout(function () {
                $target.mCustomScrollbar(options);
            }, 0);
        });
    }
}


var selectRegions = {
    target: function () {
        return $('.js-select-regions');
    },
    options: {
        closeOnSelect: false,
        allowClear: true,
        multiple: true,
        theme: 'default',
        tags: false,
        data: []
    },
    template: function (result) {
        return result.map((res) => {
            return '<option>' + res.text + '</option>'
        }).join(' ');
    },
    ajax: function () {
        $.ajax({
            type: 'get',
            url: './api/regions.json',
            dataType: 'json',
            templateResult: selectRegions.res,
            success: function (data) {
                var result = data.regions;
                selectRegions.template(result);
                selectRegions.options.data = result;
                selectRegions.init();
            },
        });
    },
    res: '',
    init: function () {
        $.fn.select2.defaults.set('language', 'ru');
        selectRegions.target().select2(selectRegions.options);
    },
    update: function () {
    }
}


var formMainSelect = {
    init: function () {
        //localization 
        $.fn.select2.defaults.set('language', 'ru');
        // $.fn.select2.amd.require(["optgroup-data", "optgroup-results"]);
        $.getJSON('./api/countries.json', function (data) {
            $.each(data, function (key, val) {
                $select.append('<input type="checkbox">' + val.title + '</input><option value="' + val.title + '">' + val.title + '</option>');
            });
        });
        var options = {
            minimumInputLength: -1,
            closeOnSelect: false,
            selectOnClose: false,
            allowClear: true,
            multiple: true,
            theme: 'default',
            tags: false,
            // data: result,
        }
        var opt_add = {
            dropdownAutoWidth: true,
            allowClear: false,
            theme: 'default select-group-container',
            // matcher: function(term, text, option) {
            //     return text.toUpperCase().indexOf(term.toUpperCase())>=0 || option.val().toUpperCase().indexOf(term.toUpperCase())>=0;
            // }
        }

        var $select = $('.js-select-search');
        var $select_custom = $('.select-search-custom');
        // var $select_regions = $('.js-select-regions');


        $select.each(function (i, el) {
            $(this).select2(options);
            // $(this).select2().on('select2:open', function(){
            // $('.select2-search__field').attr('placeholder', 'Поиск...');
            // });
        });

        $select_custom.each(function (i, el) {
            Object.assign(options, opt_add);
            $(this).select2(options);
        });
    },
}



function addSpaces(num) {
    var n = num.toString();
    return n.replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + ' ');
}


//TODO: merge 2 input number's validations, rewrite with 1 function

var priceInputsValidation = {
    init: function () {
        var $form = $('.js-form-main');
        var $price = $('.price-input');
        var $number = $('.js-field-number');
        if ($form) {
            priceInputsValidation.pattern($number);
            priceInputsValidation.pattern($price);
        }
    },
    pattern: function (inputs) {
        inputs.each(function (element, key, value) {
            setInputFilter(key, function (value) {
                return /^\d*\.?\d*$/.test(value);
            });
        });
    },
    update: function () {
    }
}


var inputReset = {
    init: function () {
        var $btn = $('.js-input-reset');
        $btn.on('click', function () {
            $(this).parent().find('input').val('');
        });
    }
}


var checkboxStateToggle = {
    init: function () {
        $('.field__checkbox').on('click', function () {
            var checked = $(".field__checkbox").is(":checked");
            if (checked) {               
                $(this).attr("checked", '').removeClass('active');
            } else {
                $(this).attr("checked", checked).addClass('active');
            }
            checkboxStateToggle.update();
        });
    },
    update: function () {

    }
}



var stateTogglerChange = {
    init: function () {
        var $toggler = $('.js-view-toggler');
        var data_type = $toggler.attr("data-type");
        if ($toggler) {
            $toggler.on('click', function () {
                $(this).toggleClass('isSelected');
            });
        }
        // isSelected

        var elem = document.querySelector('.js-view-toggler');
        if (elem !== null) {
            // var switchery = new Switchery(elem, {
            //     color: '#ffffff',
            //     secondaryColor: '#ffffff',
            //     jackColor: '#0252af',
            //     jackSecondaryColor: '#0252af'
            // });
            var changeCheckbox = document.querySelector('.js-view-toggler');
            changeCheckbox.onchange = function () {
                $('.category-list').toggle();
                $('.on-map').toggle();
                $('#list-view-toggle').toggleClass('active');
                $('#map-view-toggle').toggleClass('active');

                $(function () {
                    window.mapInit();
                });
            };
        } else {
            $('.switch-ul').hide();
        }
    }
}


var formFiltersReset = {
    init: function () {
        var $form = $('.form-main');
        var $field = $form.find($('.form-main__field input'));
        var $select = $form.find($('select'));
        var $reset = $('.js-tool-filter-reset');
        if ($form) {
            $reset.on('click', function () {
                $field.each(function (element, key, value) {
                    if (this.value !== '') {
                        this.value = '';
                    }
                });
                $select.each(function (element, key, value) {
                    if (this.value !== '') {
                        this.value = '';
                    }
                });
            });
        }
    }
}


var multiSelect = {
    init: function(){
    function templateSelection(data) {
        return data.text;
    }
    function templateResult(data) {
        if (!data.id) return data.text; // optgroup
        var id = 'state' + data.id.toLowerCase();
        var label = $('<label></label>', { for: id })
                .text(data.text);
        var checkbox = $('<input type="checkbox">', { id: id });
        
        return checkbox.add(label);   
    }
    $('#js-multi-select').select2({
        closeOnSelect: false,
        multiple: true,
        templateSelection: templateSelection,
        templateResult: templateResult,
        ajax: {
            type: 'get',
            url: './api/regions.json',
            dataType: 'json',
            success: function (data) {
                result = data.regions;
                var res = result.forEach(function(el, id){
                    return el.text;
                });      
                return res;
            },
        },
        escapeMarkup: function (m) {
            return m;
        },
        });
    }   
}


var formMainSidebar = {
    init: function () {
        var $form = $('.js-form-main');
        if ($form) {
            $(window).on('scroll', debounce(250, function () {
                var $formHeight = $form.outerHeight();
                var $formPosition = $form[0].getBoundingClientRect().bottom;
                if ($formPosition < 0) {
                    $form.addClass('isSticky');
                    document.querySelector('.js-form-main').style.transitionDuration = "0.3s";
                    document.querySelector('.js-form-main').style.transform = `translateY(${$form.outerHeight() + 'px'})`;
                } else {
                    $form.removeClass('isSticky');
                }
            }));

        }
    }
}


/**
 * @param  { jQuery } target
 * @param  { Integer } delay
 */
class formMainObserver {
    constructor(target, delay, inView){
        this.target = target;
        this.delay = delay;
        this.inView = inView;
        if(this.target.length > 0){
            this.observer();
        }
    }
    observer(){
        let pageTop = $(window).scrollTop();
        let pageBottom = pageTop + $(window).height();
        let targetTop = this.target.offset().top;
        let targetBottom = targetTop + this.target.height();
        $(document).on('scroll',  debounce(this.delay, () => {
            if (this.inView === true) {
                return ((pageTop < targetTop) && (pageBottom > targetBottom));
            } else {
                return ((targetTop <= pageBottom) && (targetBottom >= pageTop));
            }
        }));
    } 
} 

    
class formSticky {
    constructor(parent, target){
        this.parent = parent;
        this.target = target;
        this.options = {
            parent: $('.catalog-card-group__container'),
            offset_top: 25,
            inner_scrolling: false,
            sticky_class: 'isSticky',
            spacer: ".sticky-spacer"
        },
        this.stickyInit();
    }
    stickyInit(){
        if($('.form-main--sidebar').length > 0){
            $('.form-main--sidebar').stick_in_parent({
                parent: $('.catalog-card-group__container'),
                offset_top: 25,
                inner_scrolling: false,
                sticky_class: 'isSticky',
                spacer: ".form-spacer"
            });
            $('.form-main--sidebar').trigger("sticky_kit:recalc");
        }
    }
}
new formSticky($('.form-main--sidebar .js-form-observer'), $('.form-main__body'));


class fSelectSticky {
    constructor(child){
            this.childId =  child.attr('id');
            this.child = child.parent('.fake-select__container');
            this._delta = 0;
            this.parent = $(`.js-select-parent[data-child=${ this.childId }]`); // get el with current childId
            this.options = {
                theme: "dark-thick",
                snapAmount: 1,
        };
        // methods init
        if(this.child.length > 0 && this.parent.length > 0){ 
            // get position info
            this.getChildPosition();
            this.getParentPosition();
            // set position info
            // this.setChildPosition();
            // ckick event on parent listener
            this.click();
            // child inner csroll init
            this.childInnerScroll();
        }

    }
    click(){
        // click on parent element
        let self = this;
        self.parent.on('click', function()  {
            self.setChildPosition();          
            $(this).find('.js-state-arrow').toggleClass('isOpened');
            self.child.toggleClass('isActive');            
        });
        // click outside content element 
        $(document).mouseup((e) => { 
            if (!self.child.is(e.target) && $(e.target).closest(self.child).length === 0 && $(e.target).closest(self.parent).length === 0) { 
                self.child.removeClass('isActive');
                self.parent.find('.js-state-arrow').removeClass('isOpened');
            } 
        }); 
    }
    getChildPosition(){
        let self = this; 
        let childPosition = {
            top:  self.child.offset().top + self.child.outerHeight(),
            left: self.child.offset().left,
        };
        return childPosition;
    }
    getParentPosition(){    
        let self = this;    
        let parentPosition = {
            top:  self.parent.offset().top + self.parent.outerHeight(),
            left: self.parent.offset().left,
        };        
        return parentPosition;   
    }
    setChildPosition(){
        let self = this;
        self.child.css({
            top: self.getParentPosition().top,
            left: self.getParentPosition().left,
        });      
    }
    childInnerScroll(){
        // mCustomScrollbar render
        this.child.mCustomScrollbar('destroy');
        setTimeout(() => {
            this.child.mCustomScrollbar(this.options);
        }, 0);
    }
}


/**
 * @param  { Array } instanseList // array of fSelectSticky instances
*/

function formBodyScrollInit(instanseList) { 
    $('.form-main__body').mCustomScrollbar(
        {
        theme: "dark-thick",
        callbacks: {
            whileScrolling: function(){
                instanseList.forEach(item => { 
                    if(item.parent.length > 0){
                        item._delta = this.mcs.top;
                        item.setChildPosition();
                    }
                        calendarPublication.updatePosition();
                        calendarRealization.updatePosition();
                });
            }
        }
    });
    $(document).on('scroll', () => {
        instanseList.forEach(item => { 
            if(item.parent.length > 0 && item.child.is(':visible')){
                item.setChildPosition();
            }
                calendarPublication.updatePosition();
                calendarRealization.updatePosition();
        });
    });
}





class fSelectFactory{
    // fun=()=>{}
    static getRecount(){
        if($('.js-form-lots').length > 0){
            this.getRecountSales();
        } else {
            this.getRecountPreSales();
        }
    }
    static getRecountPreSales(){
        $.ajax({
            type: 'post',
            url: '/include/ajax/get_assets_filter_recount.php',
            dataType: 'json',
            data: recount_filter_data,
            success: (data) => {
                recount = data;                 
                this.setRecount(recount);
                // fun()
            }
        });
    }
    static getRecountSales(){
        $.ajax({
            type: 'post',
            url: ' /include/ajax/get_lots_filter_recount.php',
            dataType: 'json',
            data: recount_filter_data,
            success: (data) => {
                recount = data;                 
                this.setRecount(recount);
                // fun(data)
            }
        });
    }
    static setRecount(recount){

        if(typeof(recount['lot_section_id']) !== 'undefined'){
            recount['lot_section_id'].forEach(item => {
                if(typeof(filter_data) !== 'undefined'){
                    let activeElement = filter_data.find(x => x.id === Number(item.id));
                    activeElement.value = item.value;
                    activeElement.novelty = item.novelty;
                    selectPropertyComponent = new fSelectController(filter_data, $('#js-select-property'));
                }
            });
        }
        // if(typeof(recount['fo_id']) !== 'undefined'){
        //     recount['fo_id'].forEach(item => {
        //         let activeElement = data_realization.find(x => x.id === Number(item.id));
        //         activeElement.value = item.value;
        //         activeElement.novelty = item.novelty;
        //         selectListAgencies = new fSelectSimpleList(data_realization, $('#js-select-agency'), true, 'arrLotFilter_4');
        //     });
        // }
        // if(typeof(recount['region_id']) !== 'undefined'){
        //     recount['region_id'].forEach(item => {
        //         let activeElement = filter_data.find(x => x.id === Number(item.id));
        //         activeElement.value = item.value;
        //         activeElement.novelty = item.novelty;
        //         selectListRegions = new fSelectSimpleList(data_cities, $('#js-select-regions'), false, 'arrLotFilter_7'); 
        //     });
        // }
    }
}


$(document).ready(function () {

    // formMainSidebar.init();
    //New select obj
    customScroll.init();
    // selectRegions.ajax();
    
    // multiSelect
    // formMainSelect.init();
    // multiSelect.init();

    priceInputsValidation.init();
    checkboxStateToggle.init();
    inputReset.init();
    stateTogglerChange.init();
    formFiltersReset.init();

    selectPropertyClass =  new fSelectSticky($('#js-select-property'));
    selectAgencyClass = new fSelectSticky($('#js-select-agency'));
    selectRegionsClass = new fSelectSticky($('#js-select-regions'));
    selectRegionsSignature = new fSelectSticky($('#js-select-signature'));
    selectRegionsRealization = new fSelectSticky($('#js-select-realization'));

    calendarPublication  = new fCalendar($('.js-calendar-from-1'), $('.js-calendar-to-1'));
    calendarRealization  = new fCalendar($('.js-calendar-from-2'), $('.js-calendar-to-2'), new Date());
    
    formBodyScrollInit([selectPropertyClass, selectAgencyClass, selectRegionsClass, selectRegionsSignature, selectRegionsRealization]);

    if(typeof(filter_data) !== 'undefined'){
        new propertyParentList(filter_data);    
    }

    $('.js-calendar-icon').on('click', function(){
        $(this).parent().parent().find('.js-calendar').trigger("focus");
    });
    

    $('.form-main__field .price-input').on('focus', function(){
            $(this).siblings('.field__input-placeholder.is-left').hide();
            $(this).addClass('Removed');
            
    });

    $('.form-main__field .price-input').on('blur', function(){
            $(this).siblings('.field__input-placeholder.is-left').show();
            $(this).removeClass('Removed');
            
    });


});




