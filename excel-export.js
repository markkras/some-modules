const generateXLSX = (data)=> {
    return XlsxPopulate.fromBlankAsync()
      .then(function(workbook) {
          // регулярка для обнаружения ссылки
          const pattern = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
  
          data.forEach((el, idx) => {
            let sheet = workbook.sheet(idx);          
  
            // генерация вкладок
            if (el.name.length > 31) {
              el.name = el.name.substring(0, 31); // шапка не должна быть больше 31 символа
            }
            // удаляем символы []/\:?
            workbook.addSheet(el.name.replace(/[\[\]\/*\?/\:/']+/g, '').replace(/:/g, '.'));
  
  
            // заполнение header
            el.headers.forEach((header, i) => {
              sheet.row(1).cell(i).value(header.name);
              sheet.column(i).width(header.name.length * 1.75)
            });
  
  
            sheet.row(1).style({ bold: true, horizontalAlignment: 'center' });
  
            // заполнение остальных ячеек
            el.items.forEach((item, j) => {
              j = j + 2; // начинаем со 2й строки, т.к. в первой шапка
              item.element.forEach((e, index) => {
                if (pattern.test(e.value)) {
                  sheet.row(j).cell(index).value(`${e.value}`).style({fontColor: "0563c1", underline: true}).hyperlink(`${e.value}`);
                } else {
                  if (!e.value) {
                    e.value = '';
                  }
                  sheet.row(j).cell(index).value(e.value);
                }
              });
            });
          });
  
          return workbook.outputAsync({})
      })
  };
  
  const generateBlob = (response)=> {
  
    return generateXLSX(response).then(function(blob) {
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, "torgiasv.xlsx")
        } else {
          let url = window.URL.createObjectURL(blob);
          let a = document.createElement("a");
            document.body.appendChild(a);
            a.href = url;
            a.download = "torgiasv.xlsx";
            a.click();
            window.URL.revokeObjectURL(url);
            document.body.removeChild(a)
        }
    }).catch(function(err) {
        alert(err.message || err);
        throw err
    })
  };
  
  
  
  class BuildExcel {
    constructor() {
      this.json = [];
      this.btn = {};
    }
  
    static getJson(settings) {
      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: settings.url,
        data: settings.options,
        }).done((data)=>{
          this.json = data;
          const inst = new BuildExcel();
          inst.build(this.json);
        });
    }
  
    static preSales() {
      this.btn = $('.js_export_assets_to_excel').on('click', ()=> {
        // getJson вызывает метод build в промисе
        this.getJson({
            url: AjaxUrls.preSale().url,
            options: AjaxUrls.preSale().options
        });
      });
    }
  
    static preSalesDetail() {
      this.btn = $('.js-export-active').on('click', ()=> {
        // getJson вызывает метод build в промисе
        this.getJson({
            url: AjaxUrls.preSalesDetail().url,
            options: AjaxUrls.preSalesDetail().options
        });
      });
    }
  
    static sales() {
      this.btn = $('.js-export-lot').on('click', ()=> {
        this.getJson({
            url: AjaxUrls.sales().url,
            options: AjaxUrls.sales().options
        });
      });
    }
  
    static profileNotepad() {
      this.btn = $('.js_export_lots_to_excel').on('click', ()=> {
        this.getJson({
            url: AjaxUrls.profileNotepad().url,
            options: AjaxUrls.profileNotepad().options
        });
      });
    }
  
    static salesDetail() {
      this.btn = $('.js-export-lot').on('click', ()=> {
        this.getJson({
            url: AjaxUrls.salesDetail().url,
            options: AjaxUrls.salesDetail().options
        });
      });
    }
  
    static search() {
      this.btn = $('.js_export_lots_to_excel').on('click', ()=> {
        this.getJson({
            url: AjaxUrls.search().url,
            options: AjaxUrls.search().options
        });
      });
    }
  
    build(json) {
      generateBlob(json);
    }
    
  }
  
  class AjaxUrls {
    // в options хранятся глобальные переменные, которые объявляются шаблоне страницы
    static preSale() {
      return {
          url: '/include/ajax/get_index_actives_in_file.php',
          options: {
          iblock_id: arIBlockId,
          element_id: arElementId
        }
      }
    }
  
    static preSalesDetail() {
      return {
          url: '/include/ajax/get_active_in_file.php',
          options: {
            iblock: iblock_id.IBLOCK_ID,
            element: element_id.ACTIVE_ID,
            section: section_id.SECTION_ID
        }
      }
    }
  
    static sales() {
      return {  
          url: '/include/ajax/get_lots_in_file.php',
          options: {
            filter: salesFilter,
            sort: salesSort
          }
      }
    }
  
    static profileNotepad() {
      return {  
          url: '/include/ajax/get_lots_in_file.php',
          options: {
            filter: profileFilter,
            sort: profileSort
          }
      }
    }
  
    static salesDetail() {
      return {
          url: '/include/ajax/get_lot_in_file.php',
          options: {
            lot: salesDetail.LOT_ID
          }
      }
    }
  
    static search() {
      return {
        url: '/include/ajax/get_lots_in_file.php',
        options: {
          filter: salesFilter,
          sort: salesSort
        }
      }
    }
  }
  
  // // _________ END _________  