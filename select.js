window.firstRender = true;
/**
 * @param  { Jquery } $('#js-select-realization')
 * @param  { Array } filter_data
 * @param  { Array } recount
*/

// console.log('data', filter_data, recount_filter_data);


class fSelectController {
    constructor(data, target){
        this._selected = [];
        this.target = target;
        // create instance for each mapped element
        this.elements = data.map(element => new fSelectElement(element, this));
        // create instance for list of all items
        this.list = new fSelectList(this.elements);
        // insert result into target element
        $(this.target).html(this.render());
        this.checkboxListener();
        // setTimeout(() => {
            // this.selectedInject();
        // }, 3000);   
    }
    render(){
        // render lists with elements
        return this.list.render();
    }
    update(){
        $(this.target).html(this.render());
    }
    checkboxListener(){
         // checkbox change event
        let self = this;
        // let arrData = {};
        $(this.target).on('change', '.js-field-checkbox', function(){           
                window.firstRender = false;
                fSelectFactory.getRecount();
                 // get changed checkbox's data-id
                 let id = $(this).attr('data-id');
                 let parentId = $(this).closest('.js-select-content').attr('id');
                 let name = $(`.js-select-parent[data-child=${parentId}]`).find('input').attr('name'); // get el with current childId
                 // setting global store object
                 setFilterStore(); 
                 // current id
                 let item = self.elements.find(x => x.id === Number(id));             
                 item.checked = $(this).is(":checked");
                 // children ids
                 let triggerList = self.elements.filter(x => x.parent_id === Number(id) || x.checked === true);
                 // set props checked to true for active elements
                 triggerList.forEach(element => {
                     element.checked = true;
                 });
                 let parentList = self.elements.filter(el => el.parent_id === 0)
                 parentList.forEach(parent => {
                     if (parent.children.current_list.every(child => child.checked === true)){     
                     } else {
                         parent.checked = false;
                     }
                 });
                 let targetEl = filter_store.find(item => item.name === name);
                 // console.log(targetEl)
                 if(targetEl.link){
                     delete targetEl.link;
                 }
                 targetEl['link'] = getActiveIds(triggerList);
                 // console.log('recount_filter_data', recount_filter_data);
                 recount_filter_data['lot_section_id'] = getActiveIds(triggerList);
                 // rerender
                 $(self.target).html(self.render()); 
                 // array for input value selectedInject methods
                 self._selected = self.elements.filter(x => x.checked)
                                                 .map(y => y.name);
                self.selectedInject();              
        }); 
    }
    selectedInject(){
        let targetId = this.target.attr('id');        
        let childElement = targetId;
        let parent = $(`.js-select-parent[data-child=${ childElement }]`);
        let injectTarget = parent.find('.js-results');
        let counterTarget = parent.find('.js-results-sum'); 

        let resultSum = this._selected.length;
        if(resultSum > 0){
            counterTarget.addClass('isActive');
        } else {
            counterTarget.removeClass('isActive');
        }
        // console.log('this._selected', this._selected);
        
        let resultStr = this._selected.join(', ');
        counterTarget.text(resultSum);
        // set input's value to result string        
        injectTarget.val(resultStr);
        // trigger ajax
        injectTarget.trigger('input');
    }
    mount(){
        // NOT USED METHOD
        $(target).innerHTML = this.render();
    }
}

/**
 * @param  { Array } data
 * @param  { prop } level=1
 * @param  { prop } parent_id=null
 */
class fSelectList{
    constructor(data, level = 1, parent_id = null){
        // get current list with first level = 1 && with existing parent_id 
        this.current_list = data.filter(item => item.level === level && (parent_id ? item.parent_id === parent_id : true));
        // console.log('fikter', data);
        this.current_list = this.current_list.map(item => {
            // create children prop for every list's element && create fSelectList instance for every element, recursively
            item.children = new fSelectList(data, item.level + 1, item.id);
            return item;
        });    

    }
    update(recount) {
        // NOT USED METHOD
        if(recount){
            // делать перерендер
            this.render(recount);
        }
    }
    render() {
        // render html if the current list exists && deleting ",".
        return this.current_list.length > 0 ? `
            <ul>
                ${this.current_list.map(element => element.render()).join('')}
            </ul>
        ` : '';
    }
}


/**
 * @param  { Object } props
 *
    {
        id: 1,
        parent_id: 0,
        level: 1,
        name: 'Активы, реализуемые в процедуре ликвидации (банкротства)',
        value: 10,
        novelty: 123,
        icon: ''
    },
    /* recount Object
    {
        id: 1,
        value: 100,
        novelty: 100,
    }
 */

class fSelectElement {
    constructor(props) {
        this.id = props.id;
        this.level = props.level;
        this.name = props.name;
        this.parent_id = props.parent_id;
        this.value = props.value;
        this.children = props.children;
        this.checked = props.checked;      
    }
    render() {        
        return `
            <li class="fake-select__item ${ this.value == 0 && !window.firstRender ? 'disabled' : ''} " style="padding-left: ${this.level * 10 + 'px'}">
                <input type="checkbox" class="field__checkbox js-field-checkbox ${ this.checked ? 'active': ''}" data-value=${this.value} data-id=${this.id} ${this.checked ? 'checked' : ''}>
                <label for="${this.id}">
                    ${this.name} 
                    <span style="color: green" class="fake-select__item-value js-fake-select-value"> ${ this.value == 0 ?  '' : this.value } </span>
                    </label>
                    ${this.children ? `${this.children.render()}` : ''}
            </li>
        `;
    }
}


/**
 * @param  { Array } data
 * @param  { Jquery } target
 * @param  { Boolean } search
 * @param  { String } arrLotFilter_id

{
    id: 1,
    name: 'Агенство 1',
    value: 12
},
*/

class fSelectSimpleList {
    constructor(data, target, search, filterName){
        this._query = false;
        this._queryList = [];
        this._selected = [];
        this.target = target;
        this.search = search;
        this.filterName = filterName;
        this.state = data.map(element => new fSelectSimpleElement(element, this.filterName));       
        // this.state = this.state);
        
        $(this.target).html(this.render());
        this.checkboxListener();

        if(this.search){
            // init search input
            this.filter();
        }
    }
    checkboxListener(){
        // same as Class fSelectController checkboxListener() method, not so DRY..
        // checkbox change event
        const self = this;
        $(this.target).on('change', '.js-field-checkbox', function(){
            // get changed checkbox's data-id 
            const id = $(this).attr('data-id');
            let activeIds = [];

            // find element with this id
            const item = self.state.find(x => Number(x.id) === Number(id));
            // activeIds.push(item.id);        
            // mutate found element's "checked" state
            
            item.checked = $(this).is(":checked");
            // setFilterStore();

            // rerender
            // console.log(self._query);
            if(self._query){
                // if search exists
                self.listRerender(self._queryList);
            } else {
                // default
                $(self.target).html(self.render());
            }
            self._selected =  self.state.filter(x => x.checked)
                                        .map(y => y.name);
            self.selectedInject(); 
        }); 
    }
    selectedInject(){            
        let targetId = this.target.attr('id'); 
        let childElement = targetId;
        let parent = $(`.js-select-parent[data-child=${ childElement }]`);
        let injectTarget = parent.find('.js-results');
        let counterTarget = parent.find('.js-results-sum'); 
        let resultSum = this._selected.length;
        if(resultSum > 0){
            counterTarget.addClass('isActive');
        } else {
            counterTarget.removeClass('isActive');
        }
        let resultStr = this._selected.join(', ');
        counterTarget.text(resultSum);
        // set input's value to result string        
        injectTarget.val(resultStr);
         // trigger ajax
        injectTarget.trigger('input');
    }
    filter(){
        let self = this;
        $(document).keyup('.js-select-find', debounce(200, function(e){
            // getting filtered list
            if(e.target.value){
                self._queryList = self.state.filter(item => item.name.toUpperCase().includes(e.target.value.toUpperCase()));
                   // setting currentQuery
                self._query = self._queryList.length > 0;
                self.listRerender(self._queryList);
            }
        }));
    }
    listRerender(filtered){
        // filtered list render
        let filteredList = filtered.map(element => element.render()).join('');
        if(filteredList.length == 0){
            $(this.target).find('.select-block__not-found').show();
        } else {
            $(this.target).find('.select-block__not-found').hide();
        }
        $(this.target).find('ul').html(filteredList);
    }
    render() {
        // render html if the current list exists && deleting ",".
        return this.state.length > 0 ? `
            ${this.search ? '<div class="select-block__search"><input type="text" class="select-block__search-input js-select-find" autocomplete="off" placeholder="Поиск..."></div><div class="select-block__not-found" style="display: none;">Совпадений не найдено</div>' : ''}
            <ul>
                ${this.state.map(element => element.render()).join('')}
            </ul>
                ` : '';
        }
    }
        

class fSelectSimpleElement{
    constructor(props, filterName){
        this.id = props.id,
        this.name = props.name,
        this.value =  props.value,
        this.checked = props.checked,
        this.active = true,
        // computed item filter name from List input String param
        this.filterNameComputed = filterName + '_' + props.id;
    }
    render(){
        return `
            <li class="fake-select__item" style="${ this.active ? '' : 'color: grey;'}" >
                <input type="checkbox" data-name="${ this.filterNameComputed }" class="field__checkbox js-field-checkbox ${ this.checked ? 'active': ''}" data-value="${this.value}" data-id="${this.id}" ${this.checked ? 'checked' : ''}>
                <label for="${this.id}">
                    ${this.name}
                </label>
                <span style="color: green" class="fake-select__item-value js-fake-select-value"> 
                    ${this.value > 0 ? this.value : '' } 
                </span>
            </li>
        `;
    }
}

let selectListAgencies, selectListRegions, selectListSignatures;
let selectControllerRealization, selectPropertyComponent, selectListComponent;

$(document).ready(function () { 


    if(typeof(data_realization) !== 'undefined'){
        selectListAgencies = new fSelectSimpleList(data_realization, $('#js-select-agency'), true, 'arrLotFilter_4');
    }

    if(typeof(data_cities) !== 'undefined'){
        selectListRegions = new fSelectSimpleList(data_cities, $('#js-select-regions'), false, 'arrLotFilter_7'); 
    }

    if(typeof(data_sign) !== 'undefined'){
        selectListSignatures = new fSelectSimpleList(data_sign, $('#js-select-signature'), false, 'arrLotFilter_12'); 
    }

    if(typeof(data_implementation) !== 'undefined'){
        selectControllerRealization = new fSelectController(data_implementation, $('#js-select-realization'));
    }

    if(typeof(filter_data) !== 'undefined'){
        selectPropertyComponent = new fSelectController(JSON.parse(JSON.stringify(filter_data)), $('#js-select-property')); 
    }

    if(typeof(filter_data) !== 'undefined'){
        selectListComponent = new fSelectList(JSON.parse(JSON.stringify(filter_data)));
    }

});

 